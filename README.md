# :space_invader: NFT Marketplace
Basic implementation of marketplace with auction functionality

**ETHERSCAN:** https://rinkeby.etherscan.io/address/0xc37424148141b92397eb4368b998cCa0223cb867#code

## Tasks
- accounts (standart hardhat task to get accounts)
- createitem (create item to list in standart sale or auction)
