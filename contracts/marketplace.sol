// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/interfaces/IERC20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "./nft.sol";

import "hardhat/console.sol";

contract Marketplace is ReentrancyGuard, AccessControl, IERC721Receiver {
    using Counters for Counters.Counter;

    Counters.Counter private _bidsCount;
    Counters.Counter private _itemIds;
    Counters.Counter private _itemsSold;
    Counters.Counter private _itemUnlisted;
    IERC20 private _token;

    uint256 public _auctionTime;
    uint256 public _minBidAmount;

    bytes32 public constant ADMIN = keccak256("ADMIN");

    enum MarketType {
        SALE, 
        AUCTION, 
        NONE
    }

    struct Item {
        uint256 itemId;
        address contractAddress;
        uint256 tokenId;
        address seller;
        address owner; 
        uint256 price;

        MarketType marketType;
        uint256 bidsCount;
        uint256 bid;
        address bidder;
        uint256 startTime;
    }

    mapping(uint256 => Item) private _items;

    event itemListed(
        uint256 indexed itemId,
        address indexed nftContract,
        uint256 indexed tokenId,
        address seller,
        uint256 price
    );

    event itemCanceled(
        uint256 indexed itemId,
        address indexed nftContract,
        uint256 indexed tokenId,
        address seller,
        uint256 price
    );

    event itemBought(
        uint256 indexed itemId,
        address indexed nftContract,
        uint256 indexed tokenId,
        address seller,
        address owner,
        uint256 price
    );

    constructor(
        address token,
        uint256 auctionTime,
        uint256 minBidAmount
    ) {
        _token = IERC20(token);
        _auctionTime = auctionTime;
        _minBidAmount = minBidAmount;

        _setupRole(ADMIN, msg.sender);
    }

    function createItem(
        address nftContract,
        uint256 tokenId,
        uint256 price,
        string memory tokenURI
    ) public nonReentrant {
        require(
            nftContract != address(0), 
            "invalid nft contract"
        );

        mint(
            nftContract,
            msg.sender,
            tokenURI
        );

        uint256 itemId = _itemIds.current();
        _items[itemId] = Item(
            itemId,
            nftContract,
            tokenId,
            msg.sender,
            address(0),
            price,
            MarketType.NONE,    // marketType
            0,                  // bidsCount
            0,                  // bid
            address(0),         // bidder
            0                   // startTime
        );

        _itemIds.increment();
        _itemUnlisted.increment();
    }

    function mint(
        address nftContract,
        address owner,
        string memory tokenURI
    ) internal {
        DHeroes(nftContract).mint(
            owner,
            tokenURI
        );
    }

    function itemNotSoldExist(uint256 itemId) internal view returns (bool) {
        return _items[itemId].contractAddress != address(0);
    }

    function listItem(uint256 itemId) public nonReentrant {
        require(itemNotSoldExist(itemId), "invalid itemId");

        Item storage item = _items[itemId];
        require(
            item.marketType == MarketType.NONE,
            "token has already listed"
        );

        DHeroes(item.contractAddress).safeTransferFrom(
            item.seller,
            address(this),
            item.tokenId
        );
        item.marketType = MarketType.SALE;

        emit itemListed(
            item.itemId,
            item.contractAddress,
            item.tokenId,
            item.seller,
            item.price
        );

        _itemUnlisted.decrement();
    }

    function buyItem(uint256 itemId) public nonReentrant {
        require(itemNotSoldExist(itemId), "invalid itemId");

        Item storage item = _items[itemId];
        require(
            item.price <= IERC20(_token).balanceOf(msg.sender),
            "not enough tokens to buy"
        );

        IERC20(_token).transferFrom(
            msg.sender,
            item.seller, 
            item.price
        );
        DHeroes(item.contractAddress).safeTransferFrom(
            address(this),
            msg.sender,
            item.tokenId
        );

        item.owner = msg.sender;
        _itemsSold.increment();

        emit itemBought(
            itemId,
            item.contractAddress,
            item.tokenId,
            item.seller,
            msg.sender,
            item.price
        );
    }

    function cancel(uint256 itemId) public nonReentrant {
        require(itemNotSoldExist(itemId), "invalid itemId");

        Item storage item = _items[itemId];
        require(
            item.marketType == MarketType.SALE,
            "item should be listed first"
        );
        require(
            item.seller == msg.sender,
            "you cannot cancel"
        );

        DHeroes(item.contractAddress).safeTransferFrom(
            address(this),
            item.seller,
            item.tokenId
        );
        item.marketType = MarketType.NONE;

        emit itemCanceled(
            item.itemId,
            item.contractAddress,
            item.tokenId,
            item.seller,
            item.price
        );

        _itemUnlisted.increment();
    }

    function listItemAuction(uint256 itemId) public nonReentrant {
        require(
            itemNotSoldExist(itemId), 
            "invalid itemId"
        );

        Item storage item = _items[itemId];
        require(
            item.marketType == MarketType.NONE,
            "token has already listed"
        );

        DHeroes(item.contractAddress).safeTransferFrom(
            item.seller,
            address(this),
            item.tokenId
        );
        item.marketType = MarketType.AUCTION;
        item.startTime = block.timestamp;
        
        emit itemListed(
            item.itemId,
            item.contractAddress,
            item.tokenId,
            item.seller,
            item.price
        );
    }

    function makeBid(
        uint256 itemId, 
        uint256 price
    ) public nonReentrant {
        require(
            itemNotSoldExist(itemId), 
            "invalid itemId"
        );

        Item storage item = _items[itemId];
        
        if(item.bid == 0) {
            require(
                item.price <= price,
                "less than minimal price"
            );

            IERC20(_token).transferFrom(
                msg.sender,
                address(this), 
                price
            );
        }
        else{
            require(
                price > item.bid,
                "less or equal to last bid"
            );

            IERC20(_token).transfer(
                item.bidder, 
                item.bid
            );

            IERC20(_token).transferFrom(
                msg.sender,
                address(this), 
                price
            );
        }

        item.bidder = msg.sender;
        item.bid = price;
        item.bidsCount += 1;
    }

    function finishAuction(
        uint256 itemId
    ) public nonReentrant {
        require(
            itemNotSoldExist(itemId), 
            "invalid itemId"
        );

        Item storage item = _items[itemId];
        require(
            item.marketType == MarketType.AUCTION,
            "item should be listed first"
        );
        require(
            item.bidder == msg.sender,
            "you are not a winner"
        );
        require(
            block.timestamp - item.startTime >= _auctionTime,
            "auction is still going"
        );

        if (item.bidsCount >= _minBidAmount) { 
            // Если количество ставок набралось
            IERC20(_token).transfer(
                item.seller, 
                item.bid
            );

            DHeroes(item.contractAddress).safeTransferFrom(
                address(this),
                msg.sender,
                item.tokenId
            );

            item.owner = msg.sender;
            _itemsSold.increment();

            emit itemBought(
                itemId,
                item.contractAddress,
                item.tokenId,
                item.seller,
                item.owner,
                item.bid
            );
        }
        else {
            IERC20(_token).transferFrom(
                address(this),
                item.bidder, 
                item.bid
            );
            DHeroes(item.contractAddress).safeTransferFrom(
                address(this),
                item.seller,
                item.tokenId
            );
        }
    }

    function cancelAuction(uint256 itemId) public nonReentrant {
        require(
            itemNotSoldExist(itemId), 
            "invalid itemId"
        );

        Item storage item = _items[itemId];
        require(
            item.marketType == MarketType.AUCTION,
            "item is not in auction"
        );
        require(
            item.seller == msg.sender,
            "you cannot cancel this auction"
        );
        require(
            block.timestamp - item.startTime >= _auctionTime,
            "auction is still going"
        );
        
        DHeroes(item.contractAddress).safeTransferFrom(
            address(this),
            item.seller,
            item.tokenId
        );

        IERC20(_token).transferFrom(
            address(this),
            item.bidder,
            item.bid
        );

        emit itemCanceled(
            item.itemId,
            item.contractAddress,
            item.tokenId,
            item.seller,
            item.price
        );
    }

    // ADMIN functions to change bid amount and auction time
    function setAuctionTime(uint256 time) public {
        require(
            hasRole(ADMIN, msg.sender), 
            "you have no permission"
        );
        _auctionTime = time;
    }

    function setMinBidAmount(uint256 bidAmount) public {
        require(
            hasRole(ADMIN, msg.sender), 
            "you have no permission"
        );
        _minBidAmount = bidAmount;
    }

    function setAdmin(address addr) public {
        require(
            hasRole(ADMIN, msg.sender), 
            "you have no permission"
        );
        _setupRole(ADMIN, addr);
    }

    // Getters for sale items and auction items
    function getSaleItems() public view returns(Item[] memory) {
        uint256 totalItems = _itemIds.current();
        uint256 unsoldItems = totalItems - _itemsSold.current() - _itemUnlisted.current();
        uint256 index = 0;

        Item[] memory items = new Item[](unsoldItems);

        for(uint256 i = 0; i < totalItems; i++){
            if(
                _items[i].owner == address(0) && 
                _items[i].marketType == MarketType.SALE
            ) {
                items[index] = _items[i];
                index++;
            }
        }

        return items;
    }

    function getAuctionItems() public view returns(Item[] memory) {
        uint256 totalItems = _itemIds.current();
        uint256 unsoldItems = totalItems - _itemsSold.current() - _itemUnlisted.current();
        uint256 index = 0;

        Item[] memory items = new Item[](unsoldItems);

        for(uint256 i = 0; i < totalItems; i++){
            if(
                _items[i].owner == address(0) && 
                _items[i].marketType == MarketType.AUCTION
            ) {
                items[index] = _items[i];
                index++;
            }
        }

        return items;
    }

    function onERC721Received(
        address operator,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) public override returns (bytes4) {
        return IERC721Receiver.onERC721Received.selector;
    }
}