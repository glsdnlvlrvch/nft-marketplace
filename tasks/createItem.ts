import { task } from "hardhat/config";


task("createitem", "Create item for sale or auction")
    .addParam("marketplace", "address of marketplace")
    .addParam("nft", "nft contract")
    .addParam("tokenid", "tokenId of NFT")
    .addParam("price", "price of NFT")
    .addParam("uri", "tokenURI of NFT token")
    .addParam("wallet", "wallet address for creating")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("Marketplace", taskArgs.marketplace);
        await contract.connect(taskArgs.wallet).createItem(
            taskArgs.nft,
            taskArgs.tokenid,
            taskArgs.price,
            taskArgs.uri
        );
    });