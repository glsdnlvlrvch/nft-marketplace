import { expect } from "chai";
import { ethers } from "hardhat";
import { Contract, Signer, BigNumber } from "ethers";


describe("Marketplace", function () {
    let token: Contract;
    let marketplace: Contract;
    let nft: Contract;

    let signers: Signer[];

    const auctionTime = 15;
    const bidAmount = 2;

    function sleep(milliseconds: Number) {
        const date = Date.now();
        let currentDate = null;

        do {
          currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    }

    beforeEach(async function () {
        signers = await ethers.getSigners();
        let addr = await signers[0].getAddress();

        let Factory1 = await ethers.getContractFactory("Token");
        token = await Factory1.deploy(
            "KZToken",
            "KZT"
        );
        await token.deployed();

        let Factory2 = await ethers.getContractFactory("Marketplace");
        marketplace = await Factory2.deploy(
            token.address,
            auctionTime,
            bidAmount
        );
        await marketplace.deployed();

        let Factory3 = await ethers.getContractFactory("DHeroes");
        nft = await Factory3.deploy(
            marketplace.address
        )
        await nft.deployed();
    });

    describe("ADMIN functions", function () {
        it("setAuctionTime", async () => {
            expect(
                await marketplace._auctionTime()
            ).to.be.eq(auctionTime);

            await marketplace.setAuctionTime(2 * auctionTime);

            await expect(
                marketplace.connect(signers[1]).setAuctionTime(1)
            ).to.be.revertedWith("you have no permission");

            expect(
                await marketplace._auctionTime()
            ).to.be.eq(2 * auctionTime);
        });

        it("setMinBidAmount", async () => {
            expect(
                await marketplace._minBidAmount()
            ).to.be.eq(bidAmount);

            await marketplace.setMinBidAmount(2 * bidAmount);

            await expect(
                marketplace.connect(signers[1]).setMinBidAmount(1)
            ).to.be.revertedWith("you have no permission");

            expect(
                await marketplace._minBidAmount()
            ).to.be.eq(2 * bidAmount);
        });

        it("setAdmin", async () => {
            await expect(
                marketplace.connect(signers[1]).setMinBidAmount(1)
            ).to.be.revertedWith("you have no permission");

            await marketplace.setAdmin(await signers[1].getAddress());

            await marketplace.connect(signers[1]).setMinBidAmount(1)

            expect(
                await marketplace._minBidAmount()
            ).to.be.eq(1);
        });
    })

    
    it("Sale", async () => {
        // Creating and listing NFT
        for (var i = 0; i < 4; i++) {
            await marketplace.createItem(
                nft.address,
                i,
                1 + i,
                "fakeURI"
            );
        }
        
        await nft.setApprovalForAll(marketplace.address, true);

        for (var i = 0; i < 3; i++) {
            await marketplace.listItem(i);
        }

        await expect(
            marketplace.listItem(5)
        ).to.be.revertedWith("invalid itemId");

        await expect(
            marketplace.listItem(0)
        ).to.be.revertedWith("token has already listed");
        
        let items = await marketplace.getSaleItems();
        expect(
            items.length
        ).to.be.eq(3);

        // Buying NFT
        const buyerAddr = await signers[2].getAddress();
        const buyer = signers[2];

        await token.reward(12, buyerAddr)
        
        await token.connect(buyer).approve(marketplace.address, 12);
        await marketplace.connect(buyer).buyItem(0);

        items = await marketplace.getSaleItems();
        expect(
            items.length
        ).to.be.eq(2);

        // Cancel NFT
        await expect(
            marketplace.cancel(3)
        ).to.be.revertedWith("item should be listed first");
        
        await expect(
            marketplace.connect(buyer).cancel(1)
        ).to.be.revertedWith("you cannot cancel")

        await marketplace.cancel(1);
        items = await marketplace.getSaleItems();
        expect(
            items.length
        ).to.be.eq(1);

        await marketplace.cancel(2);
        items = await marketplace.getSaleItems();
        expect(
            items.length
        ).to.be.eq(0);
    });

    it("Auction", async () => {
        await marketplace.createItem(
            nft.address,
            0, // itemId
            2, // price
            "fakeURI"
        );

        await nft.setApprovalForAll(marketplace.address, true);

        await expect(
            marketplace.listItemAuction(1)
        ).to.be.revertedWith("invalid itemId");

        await marketplace.listItem(0);

        await expect(
            marketplace.listItemAuction(0)
        ).to.be.revertedWith("token has already listed");
        
        // listAuction

        await marketplace.createItem(
            nft.address,
            1, // itemId
            2, // price
            "fakeURI"
        );
        
        // make bid
        const bidderAddr1 = await signers[1].getAddress();
        const bidder1 = signers[1];
        const bidderAddr2 = await signers[2].getAddress();
        const bidder2 = signers[2];

        await token.reward(10, bidderAddr1);
        await token.reward(10, bidderAddr2);

        await marketplace.listItemAuction(1);

        await token.connect(bidder1).approve(marketplace.address, 10);
        await token.connect(bidder2).approve(marketplace.address, 10);

        await expect(
            marketplace.connect(bidder1).makeBid(7, 3)
        ).to.be.revertedWith("invalid itemId");

        await expect(
            marketplace.connect(bidder2).makeBid(1, 1)
        ).to.be.revertedWith("less than minimal price");

        await marketplace.connect(bidder1).makeBid(1, 2);

        await expect(
            marketplace.connect(bidder2).makeBid(1, 2)
        ).to.be.revertedWith("less or equal to last bid");

        let items = await marketplace.getAuctionItems();
        expect(
            items.length
        ).to.be.eq(1);
        
        await marketplace.connect(bidder2).makeBid(1, 3);

        await expect(
            marketplace.connect(bidder1).finishAuction(1)
        ).to.be.revertedWith("you are not a winner");
        
        await marketplace.createItem(
            nft.address,
            2, // itemId
            3, // price
            "fakeURI"
        );

        await expect(
            marketplace.connect(bidder2).finishAuction(2)
        ).to.be.revertedWith("item should be listed first");

        await expect(
            marketplace.connect(bidder2).finishAuction(1)
        ).to.be.revertedWith("auction is still going");
        
        sleep(15000);
        
        await marketplace.connect(bidder2).finishAuction(1);
        
        items = await marketplace.getAuctionItems();
        expect(
            items.length
        ).to.be.eq(0);
    });
})
